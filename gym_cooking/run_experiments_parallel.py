import time, logging, argparse, pathlib, random
import numpy as np

# from environment import OvercookedEnvironment
# from gym_cooking.envs import OvercookedEnvironment
from recipe_planner.recipe import *
from utils.world import World
from utils.agent import RealAgent, SimAgent, COLORS
from utils.activeAgent import Agent
from utils.core import *
from misc.metrics.metrics_bag import Bag

from collections import namedtuple

import gym

NUM_SEEDS = 20


def parse_arguments():
    parser = argparse.ArgumentParser(description="Overcooked environment.")
    parser.add_argument("-p", "--parallel", default=False, action="store_true")
    parser.add_argument('layout', nargs="?", type=str, default="simple.layout")                                     
    args = parser.parse_args()
    return args

def fix_seed(seed):
    np.random.seed(seed)
    random.seed(seed)

def initialize_agents(arglist):
    real_agents = []
    with open('utils/levels/{}.txt'.format(arglist.level), 'r') as f:
        phase = 1
        recipes = []
        for line in f:
            line = line.strip('\n')
            if line == '':
                phase += 1

            # phase 2: read in recipe list
            elif phase == 2:
                recipes.append(globals()[line]())

            # phase 3: read in agent locations (up to num_agents)
            elif phase == 3:
                if len(real_agents) < arglist.num_agents:
                    loc = line.split(' ')
                    model = getattr(arglist, "model{}".format(len(real_agents)+1))
                    config = getattr(arglist, "config{}".format(len(real_agents)+1))
                    arglist.config = config
                    if model == "HAICA":
                        real_agent = Agent(
                                arglist=arglist,
                                name='agent-'+str(len(real_agents)+1),
                                id_color=COLORS[len(real_agents)],
                                recipes=recipes)
                    else:
                        real_agent = RealAgent(
                                arglist=arglist,
                                name='agent-'+str(len(real_agents)+1),
                                id_color=COLORS[len(real_agents)],
                                recipes=recipes)
                    real_agents.append(real_agent)

    return real_agents

def main_loop(arglist):
    """The main loop for running experiments."""
    print("Initializing environment and agents.")
    env = gym.envs.make("gym_cooking:overcookedEnv-v0", arglist=arglist)
    obs = env.reset()
    # game = GameVisualize(env)
    real_agents = initialize_agents(arglist=arglist)

    
    # Info bag for saving pkl files
    bag = Bag(arglist=arglist, filename=env.filename+arglist.name_addon)
    bag.set_recipe(recipe_subtasks=env.all_subtasks)

    tic = time.perf_counter()
    while not env.done():
        action_dict = {}

        for agent in real_agents:
            action = agent.select_action(obs=obs)
            action_dict[agent.name] = action

        obs, reward, done, info = env.step(action_dict=action_dict)

        # Agents
        for agent in real_agents:
            agent.refresh_subtasks(world=env.world)

        # Saving info
        bag.add_status(cur_time=info['t'], real_agents=real_agents)

    toc = time.perf_counter()
    # Saving final information before saving pkl file
    bag.set_collisions(collisions=env.collisions)
    bag.set_termination(termination_info=env.termination_info,
            successful=env.successful,
            time_taken=toc-tic)

def evaluate_level(level):
    import sys, os
    # Different combinations of ToM_K
    agent_configurations = [
                        (0,  0), (0, 0.1), (0, 0.2), (0, 0.3), (0, 0.4), (0, 0.5), (0, 0.6), (0, 0.7), (0, 0.8), (0, 0.9), (0, 1),
                                    (0.1,0.1), (0.1,0.2), (0.1,0.3), (0.1,0.4), (0.1,0.5), (0.1,0.6), (0.1,0.7), (0.1,0.8), (0.1,0.9),(0.1, 1),
                                            (0.2,0.2), (0.2,0.3), (0.2,0.4), (0.2,0.5), (0.2,0.6), (0.2,0.7), (0.2,0.8), (0.2,0.9),(0.2, 1),
                                                        (0.3,0.3), (0.3,0.4), (0.3,0.5), (0.3,0.6), (0.3,0.7), (0.3,0.8), (0.3,0.9), (0.3, 1),
                                                                    (0.4,0.4), (0.4,0.5), (0.4,0.6), (0.4,0.7), (0.4,0.8), (0.4,0.9), (0.4, 1),
                                                                                (0.5,0.5), (0.5,0.6), (0.5,0.7), (0.5,0.8), (0.5,0.9), (0.5, 1),
                                                                                            (0.6,0.6), (0.6,0.7), (0.6,0.8), (0.6,0.9), (0.6, 1),
                                                                                                        (0.7,0.7), (0.7,0.8), (0.7,0.9), (0.7, 1),
                                                                                                                    (0.8,0.8), (0.8,0.9), (0.8,1),
                                                                                                                                (0.9,0.9), (0.9,1),
                                                                                                                                            (1, 1)
                    ]
    agent_configurations = [(0, 0)]

    models = ["HAICA"] #,"bd", "dc", "fb", "up", "greedy"]
    start_layout = time.perf_counter()
    print("\n({}) Starting benchmark for level {}".format(os.getpid(), level))
    sys.stdout.flush()
    for m in models:
        start_model = time.perf_counter()
        print("\n({}) Starting to benchmark models {}".format(os.getpid(), m))
        sys.stdout.flush()
        for i in range(1,NUM_SEEDS+1):
            arglist = prepare_arglist(i)
            arglist.level = level
            arglist.model1 = m 
            arglist.model2 = m 

            start_iteration = time.perf_counter()
            print("\n({}) Starting iteration {}".format(os.getpid(), i))
            sys.stdout.flush()
            
            if m == "HAICA":
                for agent_conf in agent_configurations:
                    tmp_conf = list(agent_conf)
                    random.shuffle(tmp_conf)
                    agent_conf = tuple(tmp_conf)
                    start_agent_conf = time.perf_counter()
                    arglist.config1["parameters"]["ToM_K"] = agent_conf[0]
                    arglist.config1["parameters"]["perform_ToM"] = agent_conf[0] != None
                    arglist.config2["parameters"]["ToM_K"] = agent_conf[1]
                    arglist.config2["parameters"]["perform_ToM"] = agent_conf[1] != None
                    arglist.name_addon = str(agent_conf)
                # agent_config2.parameters["Perceive_Orders"] = False
                    main_loop(arglist)
                    print("({}) Agent conf {} took: {}".format(os.getpid(), agent_conf, time.perf_counter()-start_agent_conf))
            else:
                main_loop(arglist)

            print("({}) Iteration {} took: {}".format(os.getpid(), i, time.perf_counter()-start_iteration))
            sys.stdout.flush()

    print("({}) Layout {} took: {}\n".format(os.getpid(), level, time.perf_counter()-start_layout))
    sys.stdout.flush()

  
def prepare_arglist(seed):
    arglist = argparse.Namespace()
    arglist.level = None
    arglist.num_agents = 2
    arglist.max_num_timesteps = 100
    arglist.max_num_subtasks = 14
    arglist.seed = seed
    arglist.with_image_obs = False 
    # Delegation Planner
    arglist.beta = 1.3
    # Navigation Planner
    arglist.alpha = 0.01 
    arglist.tau = 2
    arglist.cap = 75 
    arglist.main_cap = 100
    # Visualizations
    arglist.play = False 
    arglist.record = False 

    # Models
    # Valid options: `bd` = Bayes Delegation; `up` = Uniform Priors
    # `dc` = Divide & Conquer; `fb` = Fixed Beliefs; `greedy` = Greedy
    arglist.model1 = "bd"
    arglist.model2 = "bd"
    arglist.model3 = None 
    arglist.model4 = None

    arglist.name_addon = ""
    
    arglist.config1 = {"layers": [
                            {
                                "type": "GoalLayer",
                                "name": "goal",
                                "color": "Blue"
                            },
                            {
                                "type": "IntentionLayer",
                                "name": "intention",
                                "color": "Green"
                            }
                            
                        ],
                "parameters": {
                    "my_id": 1,
                    "port": 6664,
                    "env_port": 6666,
                    "time_step": 0.01,
                    "ToM_K": 0.3,
                    "perform_ToM": False
                }
        }
    arglist.config2 = {"layers": [
                            {
                                "type": "GoalLayer",
                                "name": "goal",
                                "color": "Blue"
                            },
                            {
                                "type": "IntentionLayer",
                                "name": "intention",
                                "color": "Green"
                            }
                            
                        ],
                "parameters": {
                    "my_id": 2,
                    "port": 6664,
                    "env_port": 6666,
                    "time_step": 0.01,
                    "ToM_K": 0.3,
                    "perform_ToM": False
                }
        }
    return arglist

def evaluate_local(use_parallel=False):

    logging.basicConfig(level=logging.ERROR)

    levels=["full-divider_salad", "partial-divider_salad", "open-divider_salad", "full-divider_tomato", "partial-divider_tomato", "open-divider_tomato", "full-divider_tl", "partial-divider_tl", "open-divider_tl"]
              
    
    # agent_configurations = [
    #                         (  0,  0), 
    #                         (  0,  1), (  0,0.5), (  0,0.3), (  0,0.1),
    #                         (  1,  1), (  1,0.5), (  1,0.3), (  1,0.1),
    #                         (0.5,0.5), (0.5,0.3), (0.5,0.1),
    #                         (0.3,0.3), (0.3,0.1), (0.1,0.1)
    #                     ]
    start_total = time.time()
    if use_parallel:
        print("Using multiprocessing for the different layouts")
        import multiprocessing
        jobs = []
        with multiprocessing.Pool(4) as p:
            p.map(evaluate_level, levels)
        # for layout in layouts:
        #     p = multiprocessing.Process(target=evaluate_layout, args=(layout,))
        #     jobs.append(p)
        #     p.start()
    else:
        for level in levels:
            evaluate_level(level)
    print("Finished all conditions in {}".format(time.time()-start_total))


if __name__ == "__main__":
    

    args = parse_arguments()
    evaluate_local(args.parallel)
