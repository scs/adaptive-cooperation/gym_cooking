import argparse
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import itertools
import pickle
import sys
import re
sys.path.append("../..")
import recipe_planner


recipes = [
        "tomato",
        "tl",
        "salad"
    ]
total_num_subtasks = {
        "tomato": 3,
        "tl": 6,
        "salad": 5
    }
models = [
       "_model1-bd_model2-bd",
       "_model1-up_model2-up",
       "_model1-fb_model2-fb",
       "_model1-dc_model2-dc",
       "_model1-greedy_model2-greedy",
    ]
model_key = {
    "_model1-bd_model2-bd": "BD (Wang)",
    "_model1-up_model2-up": "UP",
    "_model1-fb_model2-fb": "FB",
    "_model1-dc_model2-dc": "D&C",
    "_model1-greedy_model2-greedy": "Greedy",
}
maps = [
        "full-divider",
        "open-divider",
        "partial-divider"
        ]
seeds = range(1,21)
agents = ['agent-1', 'agent-2', 'agent-3', 'agent-4']
agents2_optimal = {
    "open-divider": {"tomato": 15, "tl": 25, "salad": 24},
    "partial-divider": {"tomato": 17, "tl": 31, "salad": 21},
    "full-divider": {"tomato": 17, "tl": 31, "salad": 21}
}
agents3_optimal = {
    "open-divider": {"tomato": 12, "tl": 22, "salad": 15},
    "partial-divider": {"tomato": 12, "tl": 22, "salad": 16},
    "full-divider": {"tomato": 13, "tl": 24, "salad": 19}
}
time_steps_optimal = {2: agents2_optimal, 3: agents3_optimal}

ylims = {
    'time_steps': [0, 100],
    'shuffles': [0, 55],
    'priors': [0, 100],
    'completion': [0, 1],
}

ylabels = {
    'time_steps': 'Time',
    'completion': 'Completion',
    'shuffles': 'Shuffles',
    'priors': 'Time',
    'completion': 'Completion'
}


def parse_arguments():
    parser = argparse.ArgumentParser(description="for parsing")
    parser.add_argument("--num-agents", type=int, default=2, help="number of agents")
    parser.add_argument("--stats", action="store_true", default=False, help="run and print out summary statistics")
    parser.add_argument("--time-steps", action="store_true", default=False, help="make graphs for time_steps")
    parser.add_argument("--completion", action="store_true", default=False, help="make graphs for completion")
    parser.add_argument("--shuffles", action="store_true", default=False, help="make graphs for shuffles")
    parser.add_argument("--legend", action="store_true", default=False, help="make legend alongside graphs")
    return parser.parse_args()


def run_main():
    #path_pickles = '/Users/custom/path/to/pickles'
    path_pickles = os.path.join(os.getcwd(), 'picklesCCJournal')
    #path_save = '/Users/custom/path/to/save/to'
    path_save = os.path.join(os.getcwd(), 'graphs_agents{}'.format(arglist.num_agents))
    if not os.path.exists(path_save):
        os.makedirs(path_save)

    if arglist.stats:
        resAI = compute_stats_AI(path_pickles)
        resBD = compute_stats(path_pickles, arglist.num_agents)
        plot_Wang_comparison(resBD, resAI)
        return

    if arglist.time_steps:
        key = 'time_steps'
    elif arglist.completion:
        key = 'completion'
    elif arglist.shuffles:
        key = 'shuffles'
    else:
        return
    df = import_data(key, path_pickles, arglist.num_agents)
    print('done loading pickle data')
    plot_data(key, path_save, df, arglist.num_agents, legend=arglist.legend)


def plot_Wang_comparison(wang_data, haica_data):
    colors = plt.cm.tab20

    fig, ax = plt.subplots(3,3)

    x = np.arange(1)
    width = 0.3/2
    for i, m in enumerate(maps):
        for j, r in enumerate(recipes):
            scenario = "{}_{}".format(m, r)
            ax[i][j].bar(x, [np.mean(haica_data[scenario]["(0, 0)"]["success"])], #yerr=[np.std(haica_data[scenario]["(0, 0)"]["success"])/np.sqrt(len(haica_data[scenario]["(0, 0)"]["success"]))], 
                            color=colors(0), 
                            width=width, label="HAICA (SP=0)")
            ax[i][j].bar(x+width*1, [np.mean(haica_data[scenario]["optimal"]["success"])], #yerr=[np.std(haica_data[scenario]["optimal"]["success"])/np.sqrt(len(haica_data[scenario]["optimal"]["success"]))], 
                            color=colors(1), 
                            width=width, label="HAICA (opt. SPs)")
            ax[i][j].bar(x+width*2, [np.mean(wang_data[scenario]["_model1-bd_model2-bd"]["success"])],  
                            #yerr=[np.std(wang_data[scenario]["_model1-bd_model2-bd"]["success"])/np.sqrt(len(wang_data[scenario]["_model1-bd_model2-bd"]["success"]))],
                            color=colors(2), 
                            width=width,label="BD")

            ax[i][j].set_xticks([], [])
            ax[i][j].set_ylim([0,1])
            ax[i][j].ylabel = "Percentage"

    fig.text(-0.05, 0.85, "Full-Divider", va="center", rotation="vertical")
    fig.text(-0.05, 0.5, "Open-Divider", va="center", rotation="vertical")
    fig.text(-0.05, 0.18, "Partial-Divider", va="center", rotation="vertical")
    fig.text(0.15, 1.0, "Tomato", va="center")
    fig.text(0.425, 1.0, "Tomato+Lettuce", va="center")
    fig.text(0.825, 1.0, "Mixed", va="center")

    handles, labels = ax[-1][-1].get_legend_handles_labels()
    fig.legend(handles, labels, loc="upper center", ncol=4,
                bbox_to_anchor=(0.5, 0.04), fancybox=True, shadow=True)
    fig.tight_layout()
    

    plt.savefig("wangComparison.pdf", bbox_inches="tight")

    plt.show()
    pass

def get_success(data, recipe):
    success = data['was_successful']
    return 1 if success else 0

def get_time_taken(data, recipe):
    time_taken = data["time_taken"]
    return time_taken

def compute_stats(path_pickles, num_agents):
    results_layout = {}
    for model in models:
        num_shuffles = []; num_timesteps = []; num_collisions = []; frac_completed = []; successes = []; time_taken = []; time_per_step = []
        for recipe, map_, seed in itertools.product(recipes, maps, seeds):
            fname = '{}_{}_agents{}_seed{}{}.pkl'.format(map_, recipe, num_agents, seed, model)
            if os.path.exists(os.path.join(path_pickles, fname)):
                try:
                    data = pickle.load(open(os.path.join(path_pickles, fname), "rb"))
                except EOFError:
                    continue

                scenario = "{}_{}".format(map_,recipe)
                if not scenario in results_layout:
                    results_layout[scenario] = {model: {"timesteps": [],
                                                        "success": [],
                                                        "time_taken": [],
                                                        "time_per_step": []}}
                if not model in results_layout[scenario]:
                    results_layout[scenario][model] = {"timesteps": [],
                                                        "success": [],
                                                        "time_taken": [],
                                                        "time_per_step": []}
                                                        
                shuffles = get_shuffles(data, recipe)   # dict of 2 numbers
                num_shuffles += shuffles.values()
                timesteps = get_time_steps(data, recipe)
                results_layout[scenario][model]["timesteps"].append(timesteps)
                num_timesteps.append(timesteps)
                succ = get_success(data, recipe)
                results_layout[scenario][model]["success"].append(succ)
                successes.append(succ)
                episode_time = get_time_taken(data, recipe)
                time_taken.append(episode_time)
                results_layout[scenario][model]["time_taken"].append(episode_time)
                time_per_step.append(episode_time/timesteps)
                results_layout[scenario][model]["time_per_step"].append(episode_time/timesteps)
                # if data['was_successful']:
                    # num_collisions.append(get_collisions(data, recipe))
                # frac_completed.append(get_frac_completed(data, recipe))
            else:
                print('no file:', fname)
                continue


        print('{}   time steps: {:.3f} +/- {:.3f}'.format(model_key[model], np.mean(np.array(num_timesteps)), np.std(np.array(num_timesteps))/np.sqrt(len(num_timesteps))))
        print('     success: {:.3f} +/- {:.3f}'.format(np.mean(np.array(successes)), np.std(np.array(successes))/np.sqrt(len(successes))))
        print('     time_taken: {:.3f} +/- {:.3f}'.format(np.mean(np.array(time_taken)), np.std(np.array(time_taken))/np.sqrt(len(time_taken))))
        print('     time_per_step: {:.3f} +/- {:.3f}'.format(np.mean(np.array(time_per_step)), np.std(np.array(time_per_step))/np.sqrt(len(time_per_step))))
        # print('     frac_completed: {:.3f} +/- {:.3f}'.format(np.mean(np.array(frac_completed)), np.std(np.array(num_collisions))/np.sqrt(len(frac_completed))))
        # print('     collisions: {:.3f} +/- {:.3f}'.format(np.mean(np.array(num_collisions)), np.std(np.array(num_collisions))/np.sqrt(len(num_collisions))))
        # print('     shuffles: {:.3f} +/- {:.3f}'.format(np.mean(np.array(num_shuffles)), np.std(np.array(num_shuffles))/np.sqrt(len(num_shuffles))))

    return results_layout

def compute_stats_AI(path_pickles):
    entries = os.listdir(path_pickles)
    entries = [e for e in entries if "HAICA" in e]

    agent_confs = []
    for e in entries:
        matchObj = re.match("(.+)_(.+)_agents2_seed(.+)_model1-HAICA_model2-HAICA(.+).pkl", e)
        if matchObj:
            map_ = matchObj.group(1)
            recipe = matchObj.group(2)
            seed = matchObj.group(3)
            agents = matchObj.group(4)
            agent_confs.append(agents)

    results = {}
    results_layout = {}

    agent_confs = set(agent_confs)
    for agent_conf in agent_confs:
        num_shuffles = []; num_timesteps = []; num_collisions = []; frac_completed = []; successes = []; time_taken = []; time_per_step = []
        for recipe, map_, seed in itertools.product(recipes, maps, seeds):
            model = "_model1-HAICA_model2-HAICA{}".format(agent_conf)
            fname = '{}_{}_agents{}_seed{}{}.pkl'.format(map_, recipe, 2, seed, model)
            if os.path.exists(os.path.join(path_pickles, fname)):
                try:
                    data = pickle.load(open(os.path.join(path_pickles, fname), "rb"))
                except EOFError:
                    continue

                scenario = "{}_{}".format(map_,recipe)
                if not scenario in results_layout:
                    results_layout[scenario] = {agent_conf: {"timesteps": [],
                                                        "success": [],
                                                        "time_taken": [],
                                                        "time_per_step": []}}
                if not agent_conf in results_layout[scenario]:
                    results_layout[scenario][agent_conf] = {"timesteps": [],
                                                        "success": [],
                                                        "time_taken": [],
                                                        "time_per_step": []}
                
                # print("{}: {}".format(agent_conf, data))
                shuffles = get_shuffles(data, recipe)   # dict of 2 numbers
                num_shuffles += shuffles.values()
                timesteps = get_time_steps(data, recipe)
                results_layout[scenario][agent_conf]["timesteps"].append(timesteps)
                num_timesteps.append(timesteps)
                succ = get_success(data, recipe)
                results_layout[scenario][agent_conf]["success"].append(succ)
                successes.append(succ)
                episode_time = get_time_taken(data, recipe)
                time_taken.append(episode_time)
                results_layout[scenario][agent_conf]["time_taken"].append(episode_time)
                time_per_step.append(episode_time/timesteps)
                results_layout[scenario][agent_conf]["time_per_step"].append(episode_time/timesteps)
            else:
                print('no file:', fname)
                continue
        modelname = "HAICA{}".format(agent_conf)
        results[agent_conf] = {"timesteps": ( np.mean(np.array(num_timesteps)), np.std(np.array(num_timesteps))/np.sqrt(len(num_timesteps))),
                            "success":(np.mean(np.array(successes)), np.std(np.array(successes))/np.sqrt(len(successes))),
                            "time_taken": (np.mean(np.array(time_taken)), np.std(np.array(time_taken))/np.sqrt(len(time_taken))),
                            # "shuffles": (np.mean(np.array(num_shuffles)), np.std(np.array(num_shuffles))/np.sqrt(len(num_shuffles))) 
                            "time_per_step": (np.mean(np.array(time_per_step)), np.std(np.array(time_per_step))/np.sqrt(len(time_per_step))) }
    
    ordered = [k for k, v in sorted(results.items(), reverse=True, key=lambda item: item[1]["success"][0])]

    print("Results for SP=0")
    r = results["(0, 0)"]
    print('   time steps: {:.3f} +/- {:.3f}'.format(r["timesteps"][0], r["timesteps"][1]))
    print('     success: {:.3f} +/- {:.3f}'.format( r["success"][0], r["success"][1]))
    print('     time_taken: {:.3f} +/- {:.3f}'.format( r["time_taken"][0], r["time_taken"][1]))
    # print('     shuffles: {:.3f} +/- {:.3f}'.format( r["shuffles"][0], r["shuffles"][1]))
    print('     time_per_step: {:.3f} +/- {:.3f}'.format( r["time_per_step"][0], r["time_per_step"][1]))
    print("10 best fixed confs")
    for i in range(10):
        k = ordered[i]
        r = results[k]
        print('{}   time steps: {:.3f} +/- {:.3f}'.format(k, r["timesteps"][0], r["timesteps"][1]))
        print('     success: {:.3f} +/- {:.3f}'.format( r["success"][0], r["success"][1]))
        print('     time_taken: {:.3f} +/- {:.3f}'.format( r["time_taken"][0], r["time_taken"][1]))
        # print('     shuffles: {:.3f} +/- {:.3f}'.format( r["shuffles"][0], r["shuffles"][1]))
        print('     time_per_step: {:.3f} +/- {:.3f}'.format( r["time_per_step"][0], r["time_per_step"][1]))

    print("Results per layout")
    for l in results_layout:
        print("\nLayout {}:\n".format(l))
        

        res = {ac: {"timesteps": ( np.mean(np.array(results_layout[l][ac]["timesteps"])), np.std(np.array(results_layout[l][ac]["timesteps"]))/np.sqrt(len(results_layout[l][ac]["timesteps"]))),
                            "success":(np.mean(np.array(results_layout[l][ac]["success"])), np.std(np.array(results_layout[l][ac]["success"]))/np.sqrt(len(results_layout[l][ac]["success"]))),
                            "time_taken": (np.mean(np.array(results_layout[l][ac]["time_taken"])), np.std(np.array(results_layout[l][ac]["time_taken"]))/np.sqrt(len(results_layout[l][ac]["time_taken"]))),
                            # "shuffles": (np.mean(np.array(num_shuffles)), np.std(np.array(num_shuffles))/np.sqrt(len(num_shuffles))) 
                            "time_per_step": (np.mean(np.array(results_layout[l][ac]["time_per_step"])), np.std(np.array(results_layout[l][ac]["time_per_step"]))/np.sqrt(len(results_layout[l][ac]["time_per_step"]))) 
                }
                for ac in results_layout[l].keys()
            }
        r = res["(0, 0)"]
        print("Performance without Belief Resonance (SP (0,0)): ")
        print('   time steps: {:.3f} +/- {:.3f}'.format(r["timesteps"][0], r["timesteps"][1]))
        print('     success: {:.3f} +/- {:.3f}'.format( r["success"][0], r["success"][1]))
        print('     time_taken: {:.3f} +/- {:.3f}'.format( r["time_taken"][0], r["time_taken"][1]))
        # print('     shuffles: {:.3f} +/- {:.3f}'.format( r["shuffles"][0], r["shuffles"][1]))
        print('     time_per_step: {:.3f} +/- {:.3f}'.format( r["time_per_step"][0], r["time_per_step"][1]))

        ordered = [k for k, v in sorted(res.items(), reverse=True, key=lambda item: (item[1]["success"][0], 100-item[1]["timesteps"][0]))]
        for i in range(5):
            k = ordered[i]
            r = res[k]
            print('{}   time steps: {:.3f} +/- {:.3f}'.format(k, r["timesteps"][0], r["timesteps"][1]))
            print('     success: {:.3f} +/- {:.3f}'.format( r["success"][0], r["success"][1]))
            print('     time_taken: {:.3f} +/- {:.3f}'.format( r["time_taken"][0], r["time_taken"][1]))
            # print('     shuffles: {:.3f} +/- {:.3f}'.format( r["shuffles"][0], r["shuffles"][1]))
            print('     time_per_step: {:.3f} +/- {:.3f}'.format( r["time_per_step"][0], r["time_per_step"][1]))


    optimal_res = {"timesteps": [],
                    "success": [],
                    "time_taken": [],
                    "time_per_step": []}
    
    for l in results_layout:
        res = {ac: {"timesteps": ( np.mean(np.array(results_layout[l][ac]["timesteps"])), np.std(np.array(results_layout[l][ac]["timesteps"]))/np.sqrt(len(results_layout[l][ac]["timesteps"]))),
                            "success":(np.mean(np.array(results_layout[l][ac]["success"])), np.std(np.array(results_layout[l][ac]["success"]))/np.sqrt(len(results_layout[l][ac]["success"]))),
                            "time_taken": (np.mean(np.array(results_layout[l][ac]["time_taken"])), np.std(np.array(results_layout[l][ac]["time_taken"]))/np.sqrt(len(results_layout[l][ac]["time_taken"]))),
                            # "shuffles": (np.mean(np.array(num_shuffles)), np.std(np.array(num_shuffles))/np.sqrt(len(num_shuffles))) 
                            "time_per_step": (np.mean(np.array(results_layout[l][ac]["time_per_step"])), np.std(np.array(results_layout[l][ac]["time_per_step"]))/np.sqrt(len(results_layout[l][ac]["time_per_step"]))) 
                }
                for ac in results_layout[l].keys()
            }
        ordered = [k for k, v in sorted(res.items(), reverse=True, key=lambda item: (item[1]["success"][0], 100-item[1]["timesteps"][0]))]

        results_layout[l]["optimal"] = dict(results_layout[l][ordered[0]])

        optimal_res["timesteps"].extend(results_layout[l][ordered[0]]["timesteps"])
        optimal_res["success"].extend(results_layout[l][ordered[0]]["success"])
        optimal_res["time_taken"].extend(results_layout[l][ordered[0]]["time_taken"])
        optimal_res["time_per_step"].extend(results_layout[l][ordered[0]]["time_per_step"])
        
    print("\n Aggregated performance with optimal SPs: ")
    print("timesteps: {:.2f} +/- {:.2f}".format(np.mean(np.array(optimal_res["timesteps"])), np.std(np.array(optimal_res["timesteps"]))/np.sqrt(len(optimal_res["timesteps"]))))
    print("success: {:.2f} +/- {:.2f}".format(np.mean(np.array(optimal_res["success"])), np.std(np.array(optimal_res["success"]))/np.sqrt(len(optimal_res["success"]))))
    print("time_taken: {:.2f} +/- {:.2f}".format(np.mean(np.array(optimal_res["time_taken"])), np.std(np.array(optimal_res["time_taken"]))/np.sqrt(len(optimal_res["time_taken"]))))
    print("time_per_step: {:.2f} +/- {:.2f}".format(np.mean(np.array(optimal_res["time_per_step"])), np.std(np.array(optimal_res["time_per_step"]))/np.sqrt(len(optimal_res["time_per_step"]))))

    return results_layout

def import_data(key, path_pickles, num_agents):
    df = list()

    for recipe, model, map_, seed in itertools.product(recipes, models, maps, seeds):
        info = {
            "map": map_,
            "seed": seed,
            "recipe": recipe,
            'model': model_key[model],
            "dummy": 0
        }

        # LOAD IN FILE
        fname = '{}_{}_agents{}_seed{}{}.pkl'.format(map_, recipe, num_agents, seed, model)
        if os.path.exists(os.path.join(path_pickles, fname)):
            try:
                data = pickle.load(open(os.path.join(path_pickles, fname), "rb"))
            except:
                print("trouble loading: {}".format(fname))
        else:
            print('no file:', fname)
            continue

        # TIME STEPS
        if key == 'time_steps':
            time_steps = get_time_steps(data, recipe)
            print("{}: {}".format(fname, time_steps))
            df.append(dict(time_steps = time_steps, **info))

        # COMPLETION
        elif key == 'completion':
            for t in range(100):
                n = get_completion(data, recipe, t)
                df.append(dict({'t': t-1, 'n': n}, **info))

        # SHUFFLES
        elif key == 'shuffles':
            shuffles = get_shuffles(data, recipe)   # a dict
            df.append(dict(shuffles = np.mean(np.array(list(shuffles.values()))), **info))
            # for agent in agents:
            #     info['agent'] = agent
            #     df.append(dict(shuffles = shuffles[agent], **info))

    return pd.DataFrame(df)

def get_time_steps(data, recipe):
    # try:
        # first timestep at which required number of recipe subtasks has been completed
        # using this instead of total length bc of termination bug

        # return data['num_completed_subtasks'].index(total_num_subtasks[recipe])+1
        return len(data["actions"]["agent-1"])
    # except:
        # return 100

def get_completion(data, recipe, t):
    df = list()
    completion = data['num_completed_subtasks']
    try:
        end_indx = completion.index(total_num_subtasks[recipe])+1
        completion = completion[:end_indx]
    except:
        end_indx = None
    if len(completion) < 100:
        completion += [data['num_completed_subtasks_end']]*(100-len(completion))
    assert len(completion) == 100
    print("completion: ", completion)
    return completion[t]/total_num_subtasks[recipe]

def get_shuffles(data, recipe):
    # recipe isn't needed but just for consistency
    # returns a dict, although we only use average of all agents
    shuffles = {}
    for agent in data['actions'].keys():
        count = 0
        actions = data['actions'][agent]
        holdings = data['holding'][agent]
        for t in range(2, len(holdings)):
            # count how many negated the previous action
            # redundant movement
            if holdings[t-2] == holdings[t-1] and holdings[t-1] == holdings[t]:
                net_action = np.array(actions[t-1]) + np.array(actions[t])
                redundant = (net_action == [0, 0])
                if redundant.all() and actions[t] != (0, 0):
                    count += 1
                    # print(agent, t, actions[t-1], holdings[t-1], actions[t], holdings[t], actions[t+1], holdings[t+1])
            # redundant interaction
            elif holdings[t-2] != holdings[t-1] and holdings[t-2] == holdings[t]:
                redundant = (actions[t-1] == actions[t] and actions[t] != (0, 0))
                if redundant:
                    count += 1
                    # print(agent, t, actions[t-1], holdings[t-1], actions[t], holdings[t], actions[t+1], holdings[t+1])
        shuffles[agent] = count
    return shuffles

def plot_data(key, path_save, df, num_agents, legend=False):
    print('generating {} graphs'.format(key))
    hue_order = [model_key[l] for l in models]
    color_palette = sns.color_palette()
    sns.set_style('ticks')
    sns.set_context('talk', font_scale=1)

    for i, recipe in enumerate(recipes):
        for j, map_ in enumerate(maps):
            data = df.loc[(df['map']==map_) & (df['recipe']==recipe), :]
            if len(data) == 0:
                print('empty data on ', (recipe, map_))
                continue

            plt.figure(figsize=(3,3))

            if key == 'completion':
                # plot ours last
                print("data: ", data)
                hue_order = hue_order[1:] + [hue_order[0]]
                color_palette = sns.color_palette()[1:4]# + [sns.color_palette()[0]]
                ax = sns.lineplot(x = 't', y = 'n', hue="model", data=data,
                    linewidth=5, legend=False, hue_order=hue_order, palette=color_palette)
                plt.xlabel('Steps')
                plt.ylim([0, 1]),
                plt.xlim([0, 100])
            else:
                hue_order = hue_order[1:] + [hue_order[0]]
                color_palette = sns.color_palette()[1:5] + [sns.color_palette()[0]]
                sns.barplot(x='dummy', y=key, hue="model", data=data, hue_order=hue_order,\
                                palette=color_palette, ci=68).set(
                    xlabel = "",
                    xticks = [],
                    ylim = ylims[key],
                )
            plt.legend('')
            plt.gca().legend().set_visible(False)
            sns.despine()
            plt.tight_layout()

            plt.ylabel(ylabels[key])

            if recipe != 'tomato' and key == 'priors':
                plt.gca().get_yaxis().set_visible(False)
                plt.gca().spines['left'].set_visible(False)
                plt.ylabel('')

            if key == 'time_steps' or key == 'priors':
                plt.axhline(y = time_steps_optimal[num_agents][map_][recipe], ls='--', color='black')

            plt.savefig(os.path.join(path_save, "{}_{}_{}.png".format(key, recipe, map_)))
            plt.close()

            print('   generated graph for {}, {}'.format(recipe, map_))

    # Make Legend
    if arglist.legend:
        plt.figure(figsize=(10,10))
        if key == 'completion':
            sns.barplot(x = 't', y = 'n', hue="model", data=data, hue_order=hue_order, palette=color_palette, ci=68).set()
        else:
            sns.barplot(x='dummy', y=key, hue="model", data=data, hue_order=hue_order, palette=color_palette, ci=68).set(
                xlabel = "", xticks = [], ylim = [0, 1000])
        legend = plt.legend(frameon=False)
        legend_fig = legend.figure
        legend_fig.canvas.draw()
        # bbox = legend.get_window_extent().transformed(legend_fig.dpi_scale_trans.inverted())
        # legend_fig.savefig(os.path.join(path_save, 'legend.pdf'), dpi="figure", bbox_inches=bbox)
        legend_fig.savefig(os.path.join(path_save, '{}_legend_full.png'.format(key)), dpi="figure")
        plt.close()



if __name__ == "__main__":
    arglist = parse_arguments()
    run_main()


