"""
    A module for general Theory of Mind reasoning.

    This considers and switches between models of different complexity,
    making use of different (learned) heuristics and inference approaches.
"""
import logging
logger = logging.getLogger(__name__)
import itertools, math, copy

ALPHA = 0.9 # Action likelihood if other agent's action corresponds to action oneself would do
class MentalModel(object):

    def __init__(self):
        self.last_world_state = None

    def recipe_requires(self, goal, intention, recipes):
        recipe = list(filter(lambda x: x.name == goal.ID, recipes))[0]
        required = {"Tomato": ["Tomato", "Plate"],
                    "Lettuce": ["Lettuce", "Plate"],
                    "Salad": ["Tomato","Lettuce", "Plate"]}
        if intention.modifier:
            for cont in required[recipe.name]:
                if cont in intention.modifier:
                    return True
            # for c in recipe.contents:
            #     print("intention: {}, content: {}".format(intention, c.full_name))
            #     if c.full_name in intention.modifier:
            #         return True
            
        if intention.ID == "interact_with_knife":
            return True # Any goal has this
        if intention.ID == "wait":
            return True # Any goal may need this
        return False
            
        
    def compute_action_likelihood(self, world_knowledge, player_id, action, intention, goal):
        # pots = world_knowledge.get_pots()
        player = world_knowledge.get_player(player_id)
        # logger.debug("computing action likelihood for action {}, intention: {}, goal: {} (player {})".format(action, intention, goal, player))
        if not self.recipe_requires(goal, intention, world_knowledge.recipe_knowledge):
            return 0
        

        if action == None:
           #Other agent did not move -> we cannot decue anything
           return 1

        if self.last_world_state:
            pred_action, path = world_knowledge.plan_action(player, intention)

        else:
            pred_action = None

        # logger.debug("predicted action: {}".format(pred_action))

        # beta = 3
        # action_probs = {}
        # for action in ("Left", "Right", "Up", "Down", "Interact", "Wait"):
        #     action_probs[action] = math.exp(beta * world_knowledge.evaluate_action(action, intention, goal, player_id))

        # norm = sum(action_probs.values())

        # action_probs = {action: value/norm for action, value in action_probs.items()}

        # logger.debug("computed action likelihoods: {}".format(action_probs))

        # return action_probs[action]
         
        # TODO make this more fine grained and allow deviations?
        if action == pred_action:
            return ALPHA
        return 1-ALPHA

    def observation_likelihood(self, world_knowledge, player_id, intention, goal): 
        p = 1 
        # if str(goal) not in world_knowledge.orders:
        #     p *= 0.1
        
        player = world_knowledge.get_player(player_id)
        if intention.ID == "get_item" and player.holding:
            p *= 0.1

        if intention.ID == "drop_item" and (not player.holding or player.holding.full_name != intention.modifier):
            p *= 0.1

        if intention.ID == "deliver_salad" and (not player.holding or not player.holding.is_deliverable()):
            p *= 0.1

        if intention.ID == "interact_with_knife" and (not player.holding or not player.holding.needs_chopped()):
            p *= 0.1

        if intention.ID == "hand_over":
            p *= 0.1

        # if intention.modifier and not intention.modifier in world_knowledge.get_item_types() \
        #     or player["in_hands"] and not intention.modifier == player["in_hands"]["type"]:
        #     p *= 0.01

        # logger.debug("obs prop for intention {}: {}".format(intention, p))
        
        return p

    def update(self, mental_state, action, player_id, world_knowledge):
        tmp_state = dict(world_knowledge.grid)
        if self.last_world_state:
            if self.last_world_state == world_knowledge.grid:
                logger.error("States should have been different!")

            world_knowledge.grid.update(self.last_world_state)

       
        
        new_state = {var: {out: 0 for out in mental_state[var].keys()} for var in mental_state}
        # TODO take more sofisticated approach
        outcome_combinations = itertools.product(mental_state["goal"].keys(), mental_state["intention"].keys()
                        )

        # Build joint probability table (INEFFICIENT!!!)
        values = dict()
        for comb in outcome_combinations:
            goal, intention = comb
            values[comb] = (self.compute_action_likelihood(world_knowledge, player_id, 
                                                            action, intention, goal) 
                                * self.observation_likelihood(world_knowledge, player_id, intention, goal)
                                * mental_state["intention"][intention] 
                                * mental_state["goal"][goal]
                            )


        # sorted_values = sorted(values.items(), key=lambda x: x[1])
        # likelihoods = ""
        # for (goal, intention), val in sorted_values[-5:]:
        #     likelihoods += "{}:{}\n".format((goal,intention), self.compute_action_likelihood(world_knowledge, player_id, action, intention, goal))
        # s = "\n".join(["{}: {}".format(key, val) for key, val in sorted_values])
        # logger.debug("values: \n{}".format(s))
        # logger.debug("likelihoods: \n{}".format(likelihoods))
        # Split joint and normalize
        norm = sum(values.values())
        # logger.debug("Norm: {}".format(norm))
        action_prob = norm
        if norm == 0:
            norm = 1
        for comb, vals in values.items():
            goal, intention = comb 
            new_state["goal"][goal] += vals/norm 
            new_state["intention"][intention] += vals/norm

        # logger.debug("norm intentions: {}".format(sum(new_state["intention"].values())))

        # # Softmax normalization
        beta = 2
        mu = 0.1
        norm = sum(math.exp(beta*(val+mu)) for val in new_state["goal"].values())
        # norm = sum(new_state["goal"].values())
        for goal, prob in new_state["goal"].items():
            # new_state["goal"][goal] = prob/norm 
            new_state["goal"][goal] = math.exp(beta*(prob+mu))/norm

        norm = sum(math.exp(beta*(val+mu)) for val in new_state["intention"].values())
        # norm = sum(new_state["intention"].values())
        for intention, prob in new_state["intention"].items():
            # new_state["intention"][intention] = prob/norm #
            new_state["intention"][intention] = math.exp(beta*(prob+mu))/norm

        # How to store last world state?
        world_knowledge.grid = tmp_state
        self.last_world_state = dict(tmp_state)
        return new_state, action_prob


class TheoryOfMind(object):

    def __init__(self, domains):
        # Hardcoded mental model for overcooked domain
        mental_model = MentalModel()
        self.domains = dict(domains)
        self.cooperativeness = None #Beta() # Initialize inferred Cooperativeness
        self.mental_model = mental_model
        self.last_agent_pos = None
        self.last_agent_orientation = None
        self.last_agent_hands = None

        self.inferred_intentions = []


    def deduce_action(self, agent):

        return agent.action
        if self.last_agent_pos is None:
            x1,y1 = 0,0
        else:
            x1,y1 = self.last_agent_pos
        x2,y2 = agent["pos"]
        d_x, d_y = (x2-x1, y2-y1)

        if (d_x, d_y) == (0,0) and self.last_agent_orientation != tuple(agent["orientation"]):
            d_x, d_y = agent["orientation"]

        
        if (d_x, d_y) == (1,0):
            action =  "Right"
        elif (d_x, d_y) == (-1,0):
            action =  "Left"
        elif (d_x, d_y) == (0,1):
            action =  "Down"
        elif (d_x, d_y) == (0,-1):
            action =  "Up"
        elif (d_x, d_y) == (0, 0):
            if self.last_agent_hands == agent["in_hands"]:
                action =  "Wait"
            else:
                action = "Interact"
        else:
            logger.debug("deduced action is None. dx,dy: {}".format((d_x,d_y)))
            action = None

        self.last_agent_pos = (x2, y2)
        self.last_agent_hands = agent["in_hands"]
        self.last_agent_orientation = tuple(agent["orientation"])
        
        return action
        

    def update(self, mental_state, world_knowledge, own_id):
        other_player = world_knowledge.get_other_player(own_id)
        if other_player is None:
            return None
        else:
            if mental_state is None:
                # Initialize mental state for other agent
                mental_state = {
                    "goal": {goal: 1/len(self.domains["goal"]) for goal in self.domains["goal"]},
                    "intention": {key: 1/len(self.domains["intention"]) for key in self.domains["intention"]}
                }
            if mental_state["intention"] is None:
                mental_state["intention"] = {key: 1/len(self.domains["intention"]) for key in self.domains["intention"]}

        action = self.deduce_action(other_player)

        # logger.debug("deduced action: {}".format(action))

        new_state, action_prob = self.mental_model.update(mental_state, action, 
                                other_player.name, world_knowledge)

        most_likely_intention = sorted(new_state["intention"].items(), key=lambda x: x[1])[-1]
        self.inferred_intentions.append(most_likely_intention)
        return new_state

    def evaluate_last_intention(self, own_intention):
        """
            Evaluate inferred intentions against a model that would predict
            the intentions the other should take given a certain level of cooperativeness.
        """
        count = 0
        for intention in self.inferred_intentions:
            if intention[0] == own_intention:
                count += 1
        logger.info("Inferred the own intention {} times (rel {})".format(count, count/(len(self.inferred_intentions) if self.inferred_intentions else 1)))
        self.inferred_intentions = []