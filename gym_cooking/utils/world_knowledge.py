
from functools import lru_cache
from heapq import heappop, heappush
import math, random
from pickle import NONE
import logging, copy
import networkx as nx
from itertools import combinations, permutations, product
logger = logging.getLogger(__name__)

VALID_NEIGHBOURS = [(-1,0), (1,0), (0,-1), (0,1)]

INGREDIENTS = ("Onion", "Tomato")

class GridWorld(object):
    """
        A simple 2d Gridworld representation where tiles are dictionaries 
        containing information regarding the tile. Provides distance functionality.
    """

    def __init__(self, recipe_knowledge):
        self.grid = {}
        self.orders = []
        self.recipe_knowledge = recipe_knowledge
        self.shared_counter = None

    @classmethod
    def from_state(cls, state):
        res = cls()
        res.update(state)
        return res

    def update(self, state):
        # Reset cache, as things (objects or players) may have changed
        # requiring us to recompute the distances -.-
        # self.compute_distance.cache_clear()
        self.world = state

        players = self.world.sim_agents
        self.grid.clear()
        world = state.world
        for x in range(world.width):
            for y in range(world.height):
                location = (x, y)
                gs = world.loc_to_gridsquare[(x, y)]
                self.grid[location] = {"pos":location, "type":gs.name, "passable": not gs.collidable, "occupied_by": gs.holding}
        players = self.world.sim_agents
        for p in players:  
            self.grid[p.location]["occupied_by"] = p 
            self.grid[p.location]["passable"] = False 

        if self.shared_counter and self.grid[self.shared_counter.location]["occupied_by"]:
            self.shared_counter = None

    def get_other_player(self, id):
        return [p for p in self.world.sim_agents if p.name != id][0]

    def more_agents(self, player_id):
        return self.get_other_player(player_id) != None


    def players_close(self):
        players = self.world.sim_agents
        player_obj = players[0]
        other_player = players[1]
        if other_player:
            x1,y1 = player_obj.location
            x2,y2 = other_player.location
            if abs(x1-x2) + abs(y2-y2) < 4:
                return True 
        return False 
    

    def get_dynamic_objects(self):
        world = self.world.world
        res = {}
        for key in sorted(world.objects.keys()):
            if key != "Counter" and key != "Floor" and "Supply" not in key and key != "Delivery" and key != "Cutboard":
                for obj in world.objects[key]:
                    if obj.full_name not in res:
                        res[obj.full_name] = [obj]
                    else:
                        res[obj.full_name].append(obj)
        return res

    def is_delivered(self, obj):
        world = self.world.world
        for tile in world.objects["Delivery"]:
            if obj.location == tile.location:
                return True 
        return False

    def fullfilled_orders(self):
        orders = []
        world = self.world.world
        for tile in world.objects["Delivery"]:
            if tile.holding:
                if len(tile.holding) > 1:
                    orders.append("Salad")
                else:
                    if "Lettuce" in tile.holding[0].full_name:
                        return "Lettuce"
                    else:
                        return "Tomato"
        return orders


    def delivery_reachable(self, player, ignore_other=False):
        delivery_tiles = self.world.world.objects["Delivery"]
        for tile in delivery_tiles:
            if self.reachable(player, tile, ignore_other=ignore_other):
                return True 
        return False


    def plan_get_item(self, player, item):
        
        objs = self.get_dynamic_objects().get(item, [])
        closest_dist = float('inf')
        closest_item = None
        for obj in objs:
            obj_loc = obj.location
            dist, p = self.compute_distance(player.location, obj_loc)
            if dist and dist < closest_dist:
                closest_dist = dist 
                closest_item = obj


        if closest_item is None:
            return None
        
        return self.deduce_action(player, closest_item.location)

    def deduce_action(self, player, target_location):

        dist, path = self.compute_distance(player.location, target_location)
        if path is None:
            return None
        next_pos = path[1]
        x,y = player.location
        d_x = next_pos[0] - x 
        d_y = next_pos[1] - y
        return (d_x, d_y)


    def plan_hand_over(self, player_obj):
        other_player = list(filter(lambda x: x.name != player_obj.name, self.world.sim_agents))[0]
        counters = self.world.world.objects["Counter"]
        best_dist = float('inf')
        counter_holding = None
        if self.shared_counter:
            counter_holding = self.grid[self.shared_counter.location]["occupied_by"]
        if not self.shared_counter or counter_holding is not None:
            best_counter = None
            for count in counters:
                if count.holding is None:
                    d1, p1 = self.compute_distance(player_obj.location, count.location)
                    d2, p2 = self.compute_distance(other_player.location, count.location)
                    if d1 != None and d2 != None and (d1+d2) < best_dist:
                        best_dist = d1+d2 
                        best_counter = count 

            if best_counter is None:
                print("could not find a counter reachable by both agents")
                return None 
            self.shared_counter = best_counter 
        return self.deduce_action(player_obj, self.shared_counter.location)

    def plan_drop_item(self, player):
        """
            New plan drop item, which will only try to drop items on a free counter space
        """
        counters = self.world.world.objects["Counter"]
        closest_counter= None
        best_dist = float("inf")

        for counter in counters:
            if counter.holding:
                continue
            dist, p = self.compute_distance(player.location, counter.location)
            if dist and dist < best_dist:
                best_dist = dist 
                closest_counter = counter
        if closest_counter is None:
            return None
        return self.deduce_action(player, closest_counter.location)



    def reachable(self, player, obj, ignore_other=False):
        ignore = []
        if ignore_other:
            other = self.get_other_player(player.name)
            ignore.append(other.location)

        dist, path = self.compute_distance(player.location, obj.location, ignore=ignore)
        return dist != None

    def plan_to_serving(self, player_obj):
        px, py = player_obj.location
        counters = self.get_serving_counter()
        servings = self.world.world.objects["Delivery"]

        best_dist = float('inf')
        best_counter = None 
        for serv in servings:
            dist, p = self.compute_distance(player_obj.location, serv.location)
            # TODO check for closest counter instead of going to the first possible
            if dist and dist < best_dist:
                best_dist = dist  
                best_counter = serv
        if best_counter is not None:
            return self.deduce_action(player_obj, best_counter.location)
        return None
        
    def get_serving_counter(self):
        counters = []
        for pos, el in self.grid.items():
            if el["type"] == "Serving":
                el["pos"] = pos
                counters.append(el)
        return counters
        
    def can_drop_on(self, tile_pos):
        if "Dispenser" in self.grid[tile_pos]["type"]:
            return False 
        if self.grid[tile_pos]["occupied_by"] != "":
            return False 
        return True

    
    def deduce_next_state(self, player, action):
        x,y = player["pos"]

        dx = dy = 0
        
        if action == "Right":
            dx,dy = 1,0
        if action == "Left":
            dx,dy = -1,0 
        if action == "Up":
            dx,dy = 0,-1
        if action == "Down":
            dx,dy = 0,1
        orientation = (dx,dy)

        if action in ("Interact", "Wait"):
            dx = dy = 0
            orientatin = player["orientation"]

        new_pos = (x+dx, y+dy)
        if not self.grid[new_pos]["passable"]:
            new_pos = (x,y)
        return new_pos, orientation
        

    def plan_action(self, player, intention, use_state=None):
        action = None

        if use_state:
            tmp_state = dict(self.grid)
            self.grid = use_state

        # intention = intention_state.sample()
        logger.debug("best intention to plan for: {}".format(intention))
        if intention.ID == "get_item":
            action = self.plan_get_item(player, intention.modifier)
        if intention.ID == "drop_item": 
            action = self.plan_drop_item(player)
        if intention.ID == "interact_with_knife":
            logger.debug("interact modifier: {}".format(intention.modifier))
            best_cut = intention.target
            
            
            if best_cut is None :
                action = (0, 0)
            else: 
                action = self.deduce_action(player, best_cut.location)
        if intention.ID == "deliver_salad":
            action = self.plan_to_serving(player)
        if intention.ID == "hand_over":
            action = self.plan_hand_over(player)
        if intention.ID == "wait":
            path = []
            if self.players_close():
                action = self.avoid(player)
            elif random.random() > 0.5:
                action = random.choice(self.world.world.NAV_ACTIONS)
            else:
                action = (0, 0)

        if use_state:
            self.grid = tmp_state
        # action = random.choice(("Up","Down","Left","Right","Interact"))
        logger.debug("selected action: {}".format(action))

        if action is None:
            return random.choice(self.world.world.NAV_ACTIONS)
        return action
    

    def avoid(self, player):
        players = self.world.sim_agents
        other = [p for p in players if p.name != player.name][0]
        player_dist, p = self.compute_distance(player.location, other.location)
        if player_dist is None:
            return (0, 0)
        away_action = None
        actions = self.world.world.NAV_ACTIONS
        random.shuffle(actions)
        for dx,dy in actions:
            x,y = player.location
            new_loc = (x+dx, y+dy)
            d, p = self.compute_distance(other.location, new_loc)
            if d and d > player_dist:
                away_action = (dx,dy)
                break
        return away_action



    def scale_distance(self, dist):
        """
            Convert the distance to a value between 0 and 1 which can
            be used as a probability later 
        """
        if dist is None:
            return 0
        # approx max distance of world
        positions = self.grid.keys()
        max_i = max_j = 0
        for pos in positions:
            max_i = max(max_i, pos[0])
            max_j = max(max_j, pos[1])
        # Take max_i for now, as the maximum distance is roughly max_i + max_j
        # and we would like to get values between 0.05 and 1
        return math.exp(-dist/(0.5*(max_i+max_j)))

    def useable(self, pot, item):
        if self.recipe_knowledge.pot_full(pot):
            return item == "Dish"
        else:
            if item == "Dish":
                return False 
            else:
                if not pot["occupied_by"] or item in pot["occupied_by"]["flavor"]:
                    return True 
                else:
                    return False

    def compute_distance(self, start, goal, direction=None, ignore=None):
        """
            Computes the distance between start and end using the A* algorithm.
            Has been adpated to overcooked, in that the goal tile does not need
            to be reached, but "touched" and faced in the end.
            
            Parameters
            ----------
            start: tuple
                Start position for the A*
            goal: tuple
                Goal position
                
            Returns
            -------
                int or None
                The distance from the start to the end position if a way can
                be found, otherwise None
                dict
                A dictionary containing the predecessor nodes for all visited nodes
                on the path. Can be used to build the path with this distance.
        """
        tiles = self.grid

        # We cannot use this shortcut as an occupied tile will not be passable and anything
        # on a counter is also not reachable. Instead we need to find a way to a neighbour of
        # these goal tiles.
        # if not tiles[start].get("passable", False) or not tiles[goal].get("passable", False):
        #     # Shortcut if either the start or goal tile are not passable
        #     return None, None

        allowed_tiles = set([goal])
        if ignore is None:
            ignore = []

        for tile in ignore:
            allowed_tiles.add(tile)

        if direction:
            dx, dy = direction
            if (start[0]+dx, start[1]+dy) == goal:
                return 0, [start, goal]
        came_from = {}
        cost_so_far = {}
        came_from[start] = None
        cost_so_far[start] = 0
        frontier = []
        heappush(frontier, (0, start))
        while len(frontier) != 0:
            current = heappop(frontier)[1]
            if current == goal:
                break

            # Add the neighbour if it is the goal tile
            passable_neighbours = [n_pos for n_pos in [(current[0]+x, current[1]+y) for x,y in VALID_NEIGHBOURS]
                                        if (tiles.get(n_pos, {}).get("passable",False) or n_pos in allowed_tiles)]
            for n_pos in passable_neighbours: 
                new_cost = cost_so_far[current] + 1
                if n_pos not in cost_so_far or new_cost < cost_so_far[n_pos]:
                    cost_so_far[n_pos] = new_cost
                    priority = new_cost + self._heuristic(goal, n_pos)
                    heappush(frontier, (priority, n_pos))
                    came_from[n_pos] = current

        dist = cost_so_far.get(goal, None)
        path = self.build_path(came_from, goal) if dist is not None else None

        # if dist is None:
        #     print("No path from {} to {} (costs: {})".format(start, goal, cost_so_far))

        return dist, path

    def build_path(self, came_from, goal):
        path = []
        cur = goal 
        while cur != None:
            path.append(cur)
            cur = came_from[cur]
        return path[::-1]

    def _heuristic(self, start, end):
        (x1, y1) = start
        (x2, y2) = end
        dist = abs(x1 - x2) + abs(y1 - y2)
        return dist

    def get_player(self, player_id):
        return [p for p in self.world.sim_agents if p.name == player_id][0]

    def evaluate_action(self, action, intention, goal, player_id):
        """
            This function is usually called by the ToM module which would first
            set the grid to a previous state
        """

        player_obj = self.get_player(player_id)
    
        other_player = self.get_other_player(player_id)
        # logger.debug("Player at {}. Other at {}".format(player_obj["pos"], other_player["pos"]))
        action_result, resulting_orientation = self.deduce_next_state(player_obj, action)

        optimal_action, optimal_path = self.plan_action(intention, player_id)

        # logger.debug("optimal action: {}".format(optimal_action))

        if action != optimal_action:
            # No need to special case "interact" since if it was not optimal to interact, 
            # interact would not have been a valid alternative. Since interact does not change position or orientation,
            # resulting distance should still be worse than doing a step
            if optimal_path:
                # wait intention will not have a path as it does not have a target
                target = optimal_path[-1]
                # recompute distance in case other player was ignored
                distance_optimal, _ = self.compute_distance(tuple(player_obj["pos"]), target, direction=tuple(player_obj["orientation"]))
                # Ignore old position
                dist, path = self.compute_distance(action_result, target, direction=resulting_orientation, ignore=[tuple(player_obj["pos"])])
                if distance_optimal is None:
                    # Optimal action ignored other agent, which blocks only path
                    distance_optimal, _ = self.compute_distance(tuple(player_obj["pos"]), target, direction=tuple(player_obj["orientation"]),
                                    ignore=[tuple(other_player["pos"])])
                    dist, path = self.compute_distance(action_result, target, direction=resulting_orientation, 
                                    ignore=[tuple(other_player["pos"]),tuple(player_obj["pos"])])
                    

                # TODO Still problematic with the potential ignoring of the other agent in case there is an alternative way.
                # The considered action may be just as good as the optimal in some cases
                # which we would miss here this way
                if distance_optimal - dist <= 0:
                    # No improvement in one step due to alternative action
                    result = -1
                else:
                    result = 1
            else:
                if optimal_action is None:
                    # No action was possible for the given intention
                    result = 0
                else:
                    result = 0.8 if action == "Wait" else 0.2 # 20% chance of perfoming a random action for intention wait

        else:
            # The same action will result in the same state, thus the same distance as the "optimal"
            result = 1


        # logger.debug("action result for {}: {}".format(action, result))

        return result

