""" layer
Represents the base class for layers of the agent's processing hierarchy.

Created on 12.12.2019

@author: skahl
"""

import sys, logging
import numpy as np
from .. import functions as fn

classes = {}
def register(cls):
    logging.debug("registering {}".format(cls.__name__))
    classes[cls.__name__] = cls
    return cls

logger = logging.getLogger(__name__)

class Layer(object):

    def __init__(self, name, log_color):
        self.name = name
        # TODO: Logger
        # self.log_color = log_color


    def __repr__(self):
        return "Layer {}".format(self.name)


    def clean_up(self):
        # TODO
        
        raise NotImplementedError("Should have implemented this")


    def finalize(self):
        # TODO
        
        raise NotImplementedError("Should have implemented this")


    def likelihood_top_down(self, domain, last_layer):
        # TODO
        
        raise NotImplementedError("Should have implemented this")


    def likelihood_bottom_up(self, domain, next_layer):
        # TODO
        
        raise NotImplementedError("Should have implemented this")

    def receive_prediction(self, top_down_likelihood, state, k=None):
        # Try using a kalman filter with equal gain to combine top down information with old prior
        if k is None:
            k = 0.5
        if abs(sum(state.P) - 1) > 0.001:
            print("State not normalized before receiving: ", state.P)
        new_state = {key: state.P_by_ID(key) + k * (top_down_likelihood[key]-state.P_by_ID(key)) for key in state.domain}
        # new_state = {key: top_down_likelihood[key]*state.P_by_ID(key) for key in state.domain}
        # norm = sum(new_state.values())
        # new_state = {k:v/norm for k,v in new_state.items()}
        # beta = 30
        # norm = sum([np.exp(beta * new_state[val]) for val in new_state])
        # new_state = {val: np.exp(beta*new_state[val])/norm for val in new_state}
        # softmax normalization
        return new_state

    def receive_evidence(self, bottom_up_likelihood, state):
        return self.receive_prediction(bottom_up_likelihood, state, k=0.5)

    # def receive_prediction(self, top_down_likelihood, state):
    #     # TODO create top-down posterior
        
    #     raise NotImplementedError("Should have implemented this")


    # def receive_evidence(self, bottom_up_likelihood, state):
    #     # TODO create bottom-up posterior
        
    #     raise NotImplementedError("Should have implemented this")


    def update(self, prediction, evidence, stats, self_state, overlap_idx2idx=None, other_state=None):
        """ Update layer state, given the current state of self and other, as well as
        the prediction and evidence reaching this layer.

        prediction and evidence are expected to be numpy arrays of probabilities.
        self_state is expected to contain only this layer's state (similarly to other_state).
        stats is expected to contain only this layer's stats.
        """


        # in case that this is the bottom-most layer
        if prediction is not None and evidence is None:
            evidence = self_state.P
        # in case that this is the top-most layer
        if prediction is None and evidence is not None:
            prediction = self_state.P

        if self_state is not None and prediction is not None and evidence is not None:

            # try:
                # calculate free energy
                F = fn.free_energy(P=prediction, Q=evidence)
                stats["free_energy"] = F[0]
                
                stats["precision"] = fn.precision(PE=evidence-prediction)
                # self.log(4, "free energy:", self.free_energy, "surprise:", F[1], "cross-entropy:", F[2])

                # check if all posteriors add up to one, and tell warning if not
                sum_bu = fn.np_sum(prediction)
                if sum_bu > 1.1 or sum_bu < 0.9:
                    logger.error("prediction not normalized: {}".format(sum_bu))
                    logger.error("prediction: {}".format(prediction))

                sum_td = fn.np_sum(evidence)
                if sum_td > 1.1 or sum_td < 0.9:
                    logger.error("evidence not normalized: {}".format(sum_td))


                # self.log(4, "calculating belief update with K =", hl_k)
                # stats["K"] = fn.kalman_gain(stats["free_energy"], stats["precision"])

                # this biases only the own K
                stats["K"] = self.set_layer_and_tomK_dependent_kalman_gain(stats)
                
                logger.debug("layer {} updated kalman gain: {:0.2f} from F({:0.2f}) and pi({:0.2f})".format(self.name, stats["K"], stats["free_energy"], stats["precision"]))

                # kalman filter update
                if other_state is not None and overlap_idx2idx is not None:
                    # allow for information from ToM beliefs to influence belief updates
                    # print("other_state belief update with Q({}) and tom_Q({})".format(evidence, other_state))

                    # other_state here only contains information about this hierarchy layer
                    K = stats["K"]
                    overlap_update_P = fn.inhibition_belief_update(P=prediction, Q=evidence, K=K, tom_Q=other_state, tom_K=stats["ToM_K"])
                    
                    
                    for idx, new_P in enumerate(overlap_update_P):
                        self_idx = overlap_idx2idx[idx]
                        self_state.P[self_idx] = new_P

                    if abs(sum(self_state.P)-1) > 0.001:
                        print("new self_state not normalized!")
                        raise AttributeError

                    # self_state.P = fn.inhibition_belief_update(prediction, evidence, stats["K"])
                    # self.hypotheses.dpd = inhibition_belief_update(self.td_posterior, self.bu_posterior, hl_k, self.personmodel)
                else:
                    # update beliefs without beliefs from ToM
                    self_state.P = fn.inhibition_belief_update(P=prediction, Q=evidence, K=stats["K"])

                if abs(sum(self_state.P) - 1) > 0.001:
                    print("self state not normalized after update: ", self_state.P)
                    raise AttributeError
            # except Exception as e:
            #     logging.error(str(e))
            #     sys.exit(1)



    def set_layer_and_tomK_dependent_kalman_gain(self, stats):
        """ bias K depending on layer name.
        belief' = td_posterior + K * (bu_posterior - td_posterior)
        """
        # if not stats["ToM_K"] is None:
        #     if self.name == "intention":
        #         # this works as follows:
        #         # - the smaller ToM_K, the stronger the gain_bias is preserved, 
        #         #      e.g., (0.8, 0.1, 0) => own K = 0.1 (preserves own prior)
        #         # - the stronger tom_K, the stronger the _new_ kalman gain influences the gain_bias, 
        #         #      e.g., (0.8, 0.1, 1) => own K = 0.8 (focus on own prediction-error)
        #         return fn.kalman_gain(stats["free_energy"], stats["precision"], 0.3) # , stats["ToM_K"])
        #     elif self.name == "goal":
        #         return fn.kalman_gain(stats["free_energy"], stats["precision"], 0.1, gain_gain=0.5) # , stats["ToM_K"])
        # else:
        return fn.kalman_gain(stats["free_energy"], stats["precision"])


    def update_params(self, stats):
        # TODO
        raise NotImplementedError("Should have implemented this")


from .intentionlayer import IntentionLayer
from .goallayer import GoalLayer

