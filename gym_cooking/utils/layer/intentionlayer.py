""" intentionlayer
Contains the class of the intention layer of the agent's processing hierarchy.

Created on 12.12.2019

@author: skahl
"""

from . import register, Layer

@register
class IntentionLayer(Layer):
    
    def __init__(self, name="intention", log_color="Green"):
        super(IntentionLayer, self).__init__(name, log_color)

        # self.receive_evidence = self.receive_prediction

    # def receive_prediction(self, top_down_likelihood, state):
    #     new_state = {key: top_down_likelihood[key]*state[key] for key in state}
    #     norm = sum(new_state.values())
    #     new_state = {k:v/norm for k,v in new_state.items()}
    #     return new_state