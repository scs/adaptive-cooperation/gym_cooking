""" iolayer
Contains the class of the bottom-most layer of the agent's processing hierarchy.

Created on 12.12.2019

@author: skahl
"""

from . import register, Layer


@register
class IOLayer(Layer):
    
    def __init__(self, name="IO", log_color="Red"):
        super(IOLayer, self).__init__(name, log_color)