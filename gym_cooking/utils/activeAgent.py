from collections import deque
import logging
logger = logging.getLogger(__name__)
import numpy as np

import random, copy 

from .tom import TheoryOfMind
from .world_knowledge import GridWorld
from .hierarchy import Hierarchy
from utils.core import mergeable
import itertools


class PersonModel(dict):
    def __init__(self):
        self.goal = {}
        self.intention = {}

    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError("No such attribute: {}".format(name))

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        if name in self:
            del self[name]
        else:
            raise AttributeError("No such attribute: {}".format(name))


class LayerState(object):

    def __init__(self, domain):
        # Move this to another function to clear without code duplication
        self._init(domain)

    def _init(self, domain):
        self.domain = list(domain)
        self.P = np.array([1/len(domain) for i in range(len(domain))])
        self.id2idx = {hypothesis:idx for idx, hypothesis in enumerate(domain)}
        self.idx2id = {idx: _id for _id, idx in self.id2idx.items()}

    def P_by_ID(self, ID):
        """ Accessing a layers probability by ID should look as follows:
        
        example:
        p = state[layer_name].P_by_ID(id)
        """
        return self.P[self.id2idx.get(ID, None)]

    def P_by_idx(self, idx):
        return self.P[idx]

    def get_MAP(self):
        max_idx = np.argmax(self.P)
        return self.domain[max_idx]

    def sample(self):
        p = random.random()
        u = 0
        for out in self.domain:
            u += self.P_by_ID(out)
            if u >= p:
                break
        return out 

    def clear(self):
        self._init([])

    def impulse(self, _id, strength):
        # Influences the given ID of the layer via the strength
        # before renormalizing
        logger.debug("give impulse of {} to {}. IDs {}. DOmain: {}".format(strength, _id, self.id2idx, self.domain))
        self.P[self.id2idx[_id]] *= strength
        self.normalize()


    def to_dict(self):
        return {
            str(_id): self.P[self.id2idx[_id]] for _id in self.id2idx
        }

    def normalize(self):
        self.P /= sum(self.P) if sum(self.P) != 0 else self.P

    def __repr__(self):
        return ", ".join(["{}: {}".format(out, val) for out,val in zip(self.domain, self.P)])

    def initialize(self, state=None):
        # (Re-)Normalize to equal distribution
        self.P = (self.P*0 + 1)/len(self.P)
        if state:
            # Update given states
            for out in state.domain:
                if out in self.domain:
                    self.P[self.id2idx[out]] = state.P[state.id2idx[out]]
            # Renormalize
            self.normalize()

    def contains(self, outcome, check_modifier=False):
        for out in self.domain:
            if check_modifier:
                if outcome == out.modifier:
                    return True 
            if outcome == out.ID:
                return True
        return False

    def clear_targets(self):
        for out in self.domain:
            out.clear()

class DomainObject(object):
    def __init__(self, hypo, modifier=None):
        self.ID = hypo
        self.modifier = modifier
        self.target = None

    def __repr__(self):
        mod = "({})".format(self.modifier) if self.modifier else ""
        return "{}{}".format(self.ID, mod)

    def clear(self):
        self.target = None

    def copy(self):
        return DomainObject(self.ID, self.modifier)

    def __eq__(self, other):
        return self.ID == other.ID and self.modifier == other.modifier

    def __ne__(self, other):
        return not (self == other)

    def __hash__(self):
        return hash(self.__repr__())


class Agent(PersonModel):

    # def __init__(self, config):
    def __init__(self, arglist, name, id_color, recipes):
        super(Agent, self).__init__()

        self.arglist = arglist
        self.name = name
        self.color = id_color
        self.recipes = recipes


        self.other = None
        self.tom = TheoryOfMind({"goal": self.goal_domain, "intention": self.full_intention_domain})
        self.layer_domains = None
        self.intended_action = None
        self.intended_intention = None
        self.location = None
        self.last_agent_orientation = None
        self.last_holding = None
        self.second_last_holding = None
        self.second_last_location = None
        self.waited = False

        self.location = None
        
        config = arglist.config
        # SETUP
        self.config = config["parameters"]
        self.hierarchy = self.configure_hierarchy(config)
        self.my_id = self.hierarchy.my_id
        self.env_id = name

        self.layer_domains = {'goal': self.goal_domain, 'intention': self.intention_domain} #self.intention_domain} # Note: Leave empty for now as we will fill it form observations
        self.state = self.prep_state(list(self.hierarchy.layername_idx_dict.keys()), 
                                                                self.layer_domains)
        print("agent state: {}".format(self.state))
        self.stats = self.prep_stats(list(self.hierarchy.layername_idx_dict.keys()))

        # Fill the other's state with all possible intentions
        self.other = {
                "goal": {goal: 1/len(self.goal_domain) for goal in self.goal_domain},
                "intention": None #{key: 1/len(self.full_intention_domain) for key in self.full_intention_domain}
            }

        self.world_knowledge = GridWorld(recipes)

        """ layer-P-dist access tests
        """
        logger.debug("Tests:\n1) print each P from layer 'goal'")
        for hypo in self.layer_domains['goal']:
            logger.debug("{}, {}".format(hypo.ID, self.state['goal'].P_by_ID(hypo)))
        logger.debug("2) print all P from layer 'intention'")
        logger.debug("intention.P: {}".format(self.state['intention'].P))


    @property
    def action(self):
        return self.intended_action 

    def clean_up(self, *args):
        logger.info("Cleaning up agent before shutting down")
        if self.use_io:
            self.io.clean_up(self.env_id)
        import os 
        os._exit(0)

    @property
    def goal_domain(self):
        return [DomainObject(str(r.name)) for r in self.recipes]

    @property
    def intention_domain(self):
        return [
            DomainObject("get_item"),
            DomainObject("interact_with_knife"),
            DomainObject("deliver_salad", "Tomato"),
            DomainObject("deliver_salad", "Lettuce"),
            DomainObject("deliver_salad", "Salad"),
            DomainObject("hand_over"), # TODO FIX
            DomainObject("drop_item"),
            DomainObject("wait")
        ]

    @property
    def full_intention_domain(self):
        return [
            DomainObject("wait"),
            DomainObject("get_item", "FreshLettuce"),
            DomainObject("get_item", "ChoppedLettuce"),
            DomainObject("get_item", "FreshTomato"),
            DomainObject("get_item", "ChoppedTomato"),
            DomainObject("get_item", "Plate"),
            DomainObject("get_item", "ChoppedLettuce-Plate"),
            DomainObject("get_item", "ChoppedTomato-Plate"),
            DomainObject("get_item", "ChoppedLettuce-Plate-ChoppedTomato"),
            DomainObject("get_item", "ChoppedTomato-Plate-ChoppedLettuce"),
            DomainObject("get_item", "ChoppedLettuce-ChoppedTomato"),
            DomainObject("get_item", "ChoppedTomato-ChoppedLettuce"),
            DomainObject("get_item", "Salad"),
            DomainObject("interact_with_knife"),
            DomainObject("drop_item", "FreshLettuce"),
            DomainObject("drop_item", "FreshTomato"),
            DomainObject("drop_item", "ChoppedLettuce"),
            DomainObject("drop_item", "ChoppedTomato"),
            DomainObject("drop_item", "Plate"),
            DomainObject("drop_item", "ChoppedLettuce-Plate"),
            DomainObject("drop_item", "ChoppedTomato-Plate"),
            DomainObject("drop_item", "ChoppedLettuce-Plate-ChoppedTomato"),
            DomainObject("drop_item", "ChoppedTomato-Plate-ChoppedLettuce"),
            DomainObject("drop_item", "ChoppedLettuce-ChoppedTomato"),
            DomainObject("drop_item", "ChoppedTomato-ChoppedLettuce"),
            DomainObject("drop_item", "Salad"),
            DomainObject("deliver_salad", "Tomato"),
            DomainObject("deliver_salad", "Lettuce"),
            DomainObject("deliver_salad", "Salad"),
            DomainObject("hand_over", "Plate"),
            DomainObject("hand_over", "ChoppedLettuce-Plate"),
            DomainObject("hand_over", "ChoppedTomato-Plate"),
            DomainObject("hand_over", "ChoppedLettuce-Plate-ChoppedTomato"),
            DomainObject("hand_over", "ChoppedTomato-Plate-ChoppedLettuce"),
            DomainObject("hand_over", "ChoppedLettuce-ChoppedTomato"),
            DomainObject("hand_over", "ChoppedTomato-ChoppedLettuce"),
            DomainObject("hand_over", "FreshTomato"),
            DomainObject("hand_over", "FreshLettuce"),
            DomainObject("hand_over", "ChoppedTomato"),
            DomainObject("hand_over", "ChoppedLettuce"),
            DomainObject("hand_over", "Salad")
        ]


    def __repr__(self):
        repr_str = "Agent hierarchy:"
        repr_str += str(self.hierarchy.layers)
        return repr_str


    def setup_player_connection(self, agent_id):
        logger.info("setting env id to: {}".format(agent_id))
        self.env_id = int(agent_id)

    def configure_hierarchy(self, config):
        hierarchy = Hierarchy(config) 
        self.my_id = hierarchy.my_id

        hierarchy.get_layer("intention").likelihood_bottom_up = self.intention_likelihoods_bu
        hierarchy.get_layer("intention").likelihood_top_down = self.intention_likelihoods_td
        hierarchy.get_layer("goal").likelihood_bottom_up = self.goal_likelihoods_bu
        hierarchy.get_layer("goal").likelihood_top_down = self.goal_likelihoods_td
        
        # ...
        # layer_1 = Layer("recipes")
        # layer_1.likelihood_top_down = self.recipe_top_down
        # layer_1.likelihood_bottom_up = self.recipe_bottom_up
        # layer.set_top_down = self.compute_likelihood
        # layer.set_bottom_up = self.compute_bottom_up_likelihood
    
        return hierarchy


    def prep_state(self, layer_names, layer_domains):
        """ Prepare state-struct that consists of layer states.
        This preparation expects a list of layer names, as well as a dict of layer domains, 
        with each domain consisting of a list of possible hypotheses.
        """
        state = {name: 
            LayerState(
                domain = layer_domains[name]
            )
            for name in layer_names
        }
        return state


    def prep_stats(self, layer_names, hist_len=10):
        stats = {name:
            {
                'free_energy': 0.,
                'transient_FE': deque(maxlen=hist_len), 
                'mean_FE': 0.,
                'precision': 0.,
                'K': 0.,
                'ToM_cooperativeness': self.config.get("ToM_cooperativeness", 0.),
                'ToM_dominance': self.config.get("ToM_dominance", 0.),
                'ToM_K': self.config.get("ToM_K", 0.),
            }
            for name in layer_names
        }

        return stats


    def send_state(self):
        """
            Serialises the current state and sends a copy out to any listener.
        """

        _state = {key: val.to_dict() for key,val in self.state.items()}
        _state["agent_id"] = self.env_id
        _state["curGoal"] = str(self.state["goal"].get_MAP())
        _state["curIntention"] = str(self.state["intention"].get_MAP())

        _otherState = {var: {str(dom): val for dom,val in self.other[var].items()} for var in self.other} if self.other else None
        state_msg = {"type": "stateMessage", "agent_id": self.env_id, "agentState{}".format(self.env_id): {"self": _state, "other": _otherState}}
        if self.use_io:
            self.io.notify_state_observers(state_msg)


    def reset_state(self):
        logger.debug("resetting state")
        for layer_state in self.state.values():
            layer_state.initialize()
        self.state["intention"].clear()
        # self.other = {var: {out: 1/len(self.other[var]) for out in self.other[var]} for var in self.other} if self.other else None
        self.other = None
        self.intended_action = None 
        self.intended_intention = None

    def _check_intentions(self):
        
        objects = self.world_knowledge.get_dynamic_objects().values()
        objects = list(itertools.chain.from_iterable(objects))
        objects = set([o.full_name for o in objects])

        intentions_to_add = objects# []

        new_domain = [] #list(self.state["intention"].domain)
        if intentions_to_add:
            for intention in self.intention_domain:
                # Create its own intention for all possible items for now
                if intention.ID in ("get_item", "drop_item", "hand_over"):
                    for item in intentions_to_add:
                        tmp_int = intention.copy()
                        tmp_int.modifier = item
                        # if not tmp_int in new_domain:
                        new_domain.append(tmp_int)  
                # Other intentions do not need to be adapted for the items
                else:
                    if not intention in new_domain:
                        new_domain.append(intention)


            self.layer_domains["intention"] = new_domain
            self.tom.domains["intention"] = new_domain

            for key in new_domain:
                if self.other and self.other["intention"] and not key in self.other["intention"]:
                    self.other["intention"][key] = 0
                
            new_state = LayerState(new_domain)
            new_state.initialize(self.state["intention"])

            self.state["intention"] = new_state


    def _update_values(self, player, intention, action):
        self.second_last_holding = self.last_holding 
        self.last_holding = player.holding 
        self.second_last_location = self.location
        self.location = player.location
        self.intended_intention = intention
        self.intended_action = action

    def select_action(self, obs):
        # print("obs: ", obs)
        observations = obs
        if obs.done():
            self.reset_state()

        self.world_knowledge.update(observations)

        self._check_intentions()

        self.cur_obs = obs

        # player = self.world_knowledge.get_player(self.env_id)
        player = list(filter(lambda x: x.name == self.name, obs.sim_agents))[0]
        self.cur_player = player
        # other_player = self.world_knowledge.get_other_player(self.env_id)
        other_player = list(filter(lambda x: x.name != self.name, obs.sim_agents))[0]
        self.cur_other = other_player
        logger.debug("Step with player {}, other {}".format(player, other_player))

        stuck, cycle = self._check_intended_action(player)

        # Consider having the ToM update to be run in its own parallel thread?
        if self.config.get("perform_ToM", True):
            self.other = self.tom.update(self.other, self.world_knowledge, self.env_id)
        else:
            self.other = None

        # logger.debug("other state: {}".format(self.other))

        orders = self.detect_orders() #list(observations["info"]["orders"])
        order_state = LayerState(domain = [DomainObject(order.name) for order in orders])
        # if self.config.get("Perceive_Orders", True) is False:
        #     # No order knowledge is equivalent of an equal distribution for all possible orders
        #     order_state = LayerState(domain = [DomainObject("Tomato"), DomainObject("Onion")])

        # Simulate
        self.state["orders"] = order_state
        self.state["world"] = observations

        # 1. Infer potential other's goals given inferred intentions and last inferred goal distribution
        # 2. Infer potential other's intention using last assumed goal and last inferred intention distribution
        # 3. Infer own goal given own last goal distribution, state and inferred other's goal (weighted with cooperativeness)
        # 4. Infer next intention given last inferred intentions, state, winning own goal and winning other's intention
        # 5. Plan and execute next own action given inferred intention for self (and potentially inferred action for other?)

        layer_states, self.stats = self.hierarchy.update(self.stats, observations, self.state, self.other, self.layer_domains)

        if self.intended_intention and player.holding != self.last_holding:

            if "item" in self.intended_intention.ID or "hand_over" in self.intended_intention.ID:
                # print("punishing agent {} intentions for {}".format(player.name, self.intended_intention.modifier))
                # Try to punish getItem and dropItem 
                for intent in self.state["intention"].domain:
                    if ("item" in intent.ID or "hand_over" in intent.ID) and intent.modifier == self.intended_intention.modifier:
                        # print("punishing agent {} intention {} for {}".format(player.name, intent, self.intended_intention.modifier))
                        self.state["intention"].impulse(intent, 0.001)
        
        del self.state["orders"]
        del self.state["world"]
        # self["goal"] = layer_states["goal"]

        
        
        # print("Current intentions for {}: {}".format(player.name, self.state["intention"]))
        action = None
        best_intention = self.state["intention"].get_MAP()

        if self.intended_intention and (self.intended_intention.ID != "wait" and self.intended_intention != best_intention and player.holding == self.last_holding):
            # Swapped intention without achieving it -> punish:
            if self.intended_intention in self.state["intention"].domain:
                print("{} punish intenton {} because it was abborted (new int: {})".format(player.name, self.intended_intention, best_intention))
                self.state["intention"].impulse(self.intended_intention, 0.01)
            # best_intention = self.state["intention"].get_MAP()

        print("best intention ({}): {}".format(self.name, best_intention))



        self.subtask = best_intention
        self.subtask_agent_names = []
        self.incomplete_subtasks = []

        action = self.world_knowledge.plan_action(player, best_intention)


        if not self.waited and (stuck and action == self.intended_action or cycle):
            if random.random() < 0.5:
                print("Wait because of stuck or cycle")
                self.waited = True
                action = (0, 0)
        else:
            self.waited = False

        self.state["intention"].clear_targets()


        logger.debug("Chose action: {} for intention: {}".format(action, best_intention))
        self._update_values(player, best_intention, action)
        return action

    def detect_orders(self):
        orders = []
        fulfilled_orders = self.world_knowledge.fullfilled_orders()
        for r in self.recipes:
            if not r.name in fulfilled_orders:
                orders.append(r)
        return orders



    def refresh_subtasks(self, **kwargs):
        pass

    def get_holding(self):
        if self.cur_player.holding is None:
            return 'None'
        return self.cur_player.holding.full_name

    def _check_intended_action(self, agent):
        
        if agent is None:
            logger.error("Could not get agent obj for id: {}. (grid: {})".format(self.env_id, self.world_knowledge.grid))

        same_pos_t1 = agent.location == self.location 
        same_pos_t2 = agent.location == self.second_last_location

        same_hands = self.last_holding == agent.holding
        same_hands_t2 = self.second_last_holding == agent.holding

        cycle = False
        stuck = False 

        if self.intended_action != (0,0) and same_pos_t1 and same_hands:
            print("{} did not move or change holding unintended".format(agent.name))
            stuck = True

        if same_pos_t2 and same_hands_t2:
            print("{} cycled without changing hands".format(agent.name))
            cycle = True

        return stuck, cycle


    def goal_likelihoods_bu(self, goal_domain, intention_state):
        # state is LayerState from intention layer
        # Computes P(goal|intention) for all goals and all intentions!
        # Returns a dict containing the "soft-evidence" likelihood for
        # each goal
        # print("all goal lik bu goal domain: {}, intention_state: {}".format(goal_domain, intention_state))
        res = {}
        for out in goal_domain:
            res[out] = sum([self.goal_likelihood_bu(out, intention)*intention_state.P_by_ID(intention) 
                                for intention in intention_state.domain])
        # Re-normalize just in case
        norm = sum(res.values())
        res = {k: v/norm for k,v in res.items()}
        return res

    def goal_likelihood_bu(self, goal, intention):
        # Computes P(goal|intention)

        # TODO consider making this smarter and more 
        # fine grained?

        if self.recipe_requires(goal, intention):
            return 1
        return 0

    def recipe_requires(self, goal, intention):
        recipe = list(filter(lambda x: x.name == goal.ID, self.recipes))[0]
        required = {"Tomato": ["Tomato", "Plate"],
                    "Lettuce": ["Lettuce", "Plate"],
                    "Salad": ["Tomato","Lettuce", "Plate"]}
        if intention.modifier:
            for cont in required[recipe.name]:
                if cont in intention.modifier:
                    return True
            
        if intention.ID == "interact_with_knife":
            return True # Any goal has this
        if intention.ID == "wait":
            return True # Any goal may need this
        return False

    def goal_likelihoods_td(self, domain, actual_order):
        # We do not need to use soft evidence here, because we get "hard" evidence in
        # the form of our observations
        res = {out: self.goal_likelihood_td(out, actual_order) for out in domain}
        
        # Re-normalize in case we have multiple orders
        norm = sum(res.values())
        res = {k: v/norm for k,v in res.items()}
        # print("Resulting goal likelihoods TD: ", res)
        return res

    def goal_likelihood_td(self, goal, order):
        # TODO consider taking the ordering of orders into account, i.e. weigh the first order higher?
        # print("goal td lik for goal {} and order {}".format(goal, order))
        return 1 if order.contains(goal.ID) else 0

    def intention_likelihoods_td(self, intention_domain, goal_state):
        # Computes the likelihood P(intention|goal), treating goal as soft
        # evidence
        norms = {}
        for goal in goal_state.domain:
            norms[goal] = sum([self.recipe_requires(goal, intention) for intention in intention_domain])

        res= {out: sum([self.recipe_requires(goal, out)/norms[goal]*goal_state.P_by_ID(goal) 
                            for goal in goal_state.domain]) for out in intention_domain}
        # print("Normalized td likelihoods: ", res)
        return res

        

    def intention_likelihoods_bu(self, intention_domain, world_state):
        """
            Computes the (unnormalized) bottom-up likelihood of the given intention.

            Possible Intentions we need to cover:

            Fine grained (without need for cooperation)
            - get first Onion/Tomato
            - get second Onion/Tomato
            - get third Onion/Tomato
            - bring first Onion/Tomato to pot
            - bring second Onion/Tomato to pot
            - bring third Onion/Tomato to pot
            - get plate
            - get soup from pot
            - deliver soup

            When cooperation is necessary additionally
            - place first/second/third Onion/Tomato at reachable position for other player
            - place plate at reachable position for other player
            - pickup soup

            Functional intentions:

            - Get_Item(Item) with item being currently Onion,Tomato,Plate,Soup
                - The goal of this intention would be to hold item in the player's hand
            - Deliver_To_pot(pot)/Interact_with_Pot(pot):
                - Interact with the specified pot with an item in the player's hands
            - Deliver_Soup
                - Requires finished dish in agent hand and way to reach serving area
            - Hand_over(item):
                - Place item somewhere the other agent can reach it (make it easier: Pick closest 
                position, reachable by both)
            - Drop_item():
                Drop the current item at the closest free counter
        """
        # return {out.ID: random.random() for out in intention_domain}
        res = {}

        player_obj = self.cur_player
        if player_obj is None:
            logger.error("Could not find player for id {}".format(self.env_id))
        for intention in intention_domain:

            # like = self.world_knowledge.likelihood(intention, self.env_id)
            if intention.ID == "get_item":
                like = self.get_item_likelihood(intention.modifier, player_obj)
            if intention.ID == "interact_with_knife":
                like = self.interact_with_knife_likelihood(intention, player_obj)
            if intention.ID == "deliver_salad":
                like = self.deliver_salad_likelihood(intention, player_obj)
            #Consider if we need the modifiers for these likelihood?
            if intention.ID == "hand_over":
                like = self.hand_over_likelihood(intention.modifier, player_obj)
            if intention.ID == "drop_item":
                like = self.drop_item_likelihood(intention.modifier, player_obj)
            if intention.ID == "wait":
                like = self.wait_likelihood(player_obj)

            res[intention] = like


        # print("unnormalized likelihoods: {}".format(res))
        # Normalize
        norm = sum(res.values())
        res = {intention: res[intention]/norm for intention in res}

        return res

    def deliver_salad_likelihood(self, intention, player_obj):
        # Would this be a possible interaction?
        if not self.world_knowledge.delivery_reachable(player_obj):
            if not self.world_knowledge.delivery_reachable(player_obj, ignore_other=True):
                return 0

        
        required = {"Tomato": ["Tomato", "Plate"],
                    "Lettuce": ["Lettuce", "Plate"],
                    "Salad": ["Tomato","Lettuce", "Plate"]}
        for r in self.recipes:
            if intention.modifier in r.name:
                # recipe is relevant
                if player_obj.holding:
                    for r in required[r.name]:
                        if not r in player_obj.holding.full_name:
                            missing = True 
                            break
                    else:
                        # All required ingredients are there.
                        return 1

        return 0

    def valid_combination(self, hold_obj, obj):
        if hold_obj is None:
            return True
        
        hold_c = copy.copy(hold_obj)
        other_c = copy.copy(obj)
        hold_c.merge(obj)
        valid_contents = {"Tomato": ["Tomato", "Plate"],
                    "Lettuce": ["Lettuce", "Plate"],
                    "Salad": ["Tomato","Lettuce", "Plate"]}
        valid_combination = False
        for r in self.recipes:
            for c in hold_c.contents:
                if not c.name in valid_contents[r.name]:
                    valid_combination = False
                    break 
            else:
                valid_combination = True
                break
            
        return valid_combination

    def get_item_likelihood(self, item, player_obj):
        # print("get_item likelihood. player ({}) item: {}, holding: {}".format(player_obj.name, item, player_obj.holding))

        world = self.cur_obs
        # Do we find the desired object at all?
        item_obj = None
        objs = self.world_knowledge.get_dynamic_objects()
        # print("all objs: ", objs)
        prob = 0
        for obj in objs.get(item, []):
            # print("obj full_name: ", obj.full_name)
            if not obj.is_held and not self.world_knowledge.is_delivered(obj):
                if self.world_knowledge.reachable(player_obj, obj):
                    # print("obj not held full_name: ", obj.full_name)
                    if player_obj.holding is None or (player_obj.holding != obj and player_obj.holding and mergeable(player_obj.holding, obj)):
                        # Check if merged object is required
                        if self.valid_combination(player_obj.holding, obj):
                            prob = 1
                            break 
                        else:
                            print("invalid combination of {} and {}".format(player_obj.holding, obj))
        return prob


    def interact_with_knife_likelihood(self, intention, player_obj):
        # Would this be a possible interaction?
        if player_obj.holding is None or not player_obj.holding.needs_chopped():
            return 0

        cutboards = self.cur_obs.world.objects["Cutboard"]
        best_cut = None 
        best_dist = float('inf')
        for cut in cutboards:
            dist, p = self.world_knowledge.compute_distance(player_obj.location, cut.location)
            if dist and dist < best_dist:
                best_dist = dist 
                best_cut = cut 

        if best_cut:
            intention.target = best_cut
            p = self.world_knowledge.scale_distance(best_dist)
            return p
        return 0


    def wait_likelihood(self, player_obj):
        return 1/len(self.state["intention"].domain)


    def hand_over_likelihood(self, item, player_obj):
        if player_obj.holding is None or not item == player_obj.holding.full_name:
            return 0

        if self.cur_other is None:
            return 0
        
        world = self.cur_obs

        if player_obj.holding.needs_chopped():
            cutboards = world.world.objects["Cutboard"]
            for cut in cutboards:
                if self.world_knowledge.reachable(player_obj, cut):
                    # Reachable
                    # dist, p = self.world_knowledge.compute_distance(player_obj.location, cut.location)
                    return 0.05 #1-self.world_knowledge.scale_distance(dist)
        if player_obj.holding.is_deliverable():
            delivery = world.world.objects["Delivery"]
            for deliv in delivery:
                if self.world_knowledge.reachable(player_obj, deliv):
                    # Reachable
                    # dist, p = self.world_knowledge.compute_distance(player_obj.location, deliv.location)
                    return 0.05 #1-self.world_knowledge.scale_distance(dist)
        prob = 1
        if self.cur_other.holding:
            prob *= 0.1            
        return prob


    def drop_item_likelihood(self, item, player_obj):
        # New, simpler likelihood
        # if player_obj.holding:
        #     print("drop item: {}. player holding: {}".format(item, player_obj.holding.full_name))
        if player_obj.holding is None or not item == player_obj.holding.full_name:
            return 0

        return 0.2 # assume in this simple world an item is always useable (not quite true for the plate)
