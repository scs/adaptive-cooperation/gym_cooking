""" hierarchy
Holds the hierarchy that structures the updating of all layers of the model.
Also handles communication between the model and the environment.

Created on 12.12.2019

@author: skahl
"""

# system modules
from collections import deque
import sys
import os
from time import time, sleep
import logging
logger = logging.getLogger(__name__)
# own modules
from .configurator import Config
from .layer import classes, Layer

import numpy as np


class Hierarchy(object):

    def __init__(self, config):
        """ Initialize the hierarchy by a config object, containing name and type dictionaries.
        """

        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
        # setup for logging from within the hierarchy
        # TODO

        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
        # main structures
        self.config = config
        self.layers = []
        self.layer_domains = {}
        self.layername_idx_dict = {}
        # layer references
        self.last_layer = None  # remember the layer that was last updated

        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
        # parameters
        self.my_id = self.config["parameters"]['my_id'] 

        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
        # apply layer config
        for config_line in self.config["layers"]:
            logger.debug("config line: {}".format(config_line))
            layer = self.layer_factory(config_line)

            if layer is not None:
                # remember index position of layer list
                self.layername_idx_dict[config_line["name"]] = len(self.layers)
                self.layers.append(layer)
            else:
                logger.error("Something went wrong in the layer factory: layer type is not known!")


    def get_layer(self, name):
        """ Access layer in layers list.
        """
        if name in self.layername_idx_dict:
            return self.layers[self.layername_idx_dict[name]]
        else:
            raise AttributeError("No such layer: {}".format(name))


    def set_layer(self, name, value):
        """ Access layer in layers list.
        """
        if name in self.layername_idx_dict:
            self.layers[self.layername_idx_dict[name]] = value
        else:
            raise AttributeError("No such layer: {}".format(name))


    def layer_factory(self, layer_config):
        """ Allows to instantiate layer objects for registered layer classes
        from their configured class names.
        """

        if layer_config["type"] in classes:
            layer = classes[layer_config["type"]]

            return layer(name=layer_config["name"], log_color=layer_config["color"])
        else:
            return None


    def update(self, stats, world_state, self_state, other_state, layer_domains):
        """ The full hierarchy update routine.
        This is a "prediction-first" update, so we update with the top most layer first,
        traversing the hierarchy down until we reach the bottom most layer.

        Send input via queue_input using {"vision": your_visual_input}
        Quit the update thread using dict {"quit": True}, using queue_control.
        Receive output via queue_output.
        """

        # per-layer pre-update cleanup
        # for layer in self.layers:
        #     layer.clean_up()

        # "Topmost layer is the order input"
        # orders = world_state["info"]["orders"]
        self.last_layer = self.get_top_down_layer()
        # Moved to agent step()
        # self_state["orders"] = [orders]
        # self_state["world"] = world_state

        logger.debug("intention layer: {}".format(self_state["intention"]))

        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
        # update step: traverse the hierarchy in order (top-down)
        for i, layer in enumerate(self.layers):
            logger.debug("Updating layer: {}".format(layer))
            next_layer = self.get_next_layer(i)  # returns None at the bottom-most layer

            _top_down = layer.likelihood_top_down(layer_domains[layer.name], self_state[self.last_layer.name])
            # print("top down: ", _top_down)
            if max(_top_down.values()) == 0:
                logger.error("top down: {}".format(_top_down))
                raise AttributeError("No positive top down")
            
            _bottom_up = layer.likelihood_bottom_up(self_state[layer.name].domain, self_state[next_layer.name])
            # if max(_bottom_up.values()) == 0:
            # print("bottom up: ", _bottom_up)
                # raise AttributeError("No positive bottum up")

            logger.debug("bottom_up: {}".format(_bottom_up))
            logger.debug("top_down: {}".format(_top_down))

            # Generate top_down and bottom_up posteriors, i.e. P(s|e) prop. P(e|s) * P(s) for e being top_down or bottom_up "evidence"
            _prediction = layer.receive_prediction(_top_down, self_state[layer.name])  # from next higher layer, or external source
            _evidence = layer.receive_evidence(_bottom_up, self_state[layer.name])  # receive evidence for this layer


            logger.debug("prediction: {}".format(_prediction))
            logger.debug("evidence: {}".format(_evidence))

            if other_state is None:
                self_prediction = np.array(list(_prediction.values()))
                if max(self_prediction) == 0:
                    # print("top down: ", _top_down)
                    # print("bottom up: ", _bottom_up)
                    raise AttributeError("No positive prediction")
                self_evidence = np.array(list(_evidence.values()))
                if max(self_evidence) == 0:
                    # print("top down: ", _top_down)
                    # print("bottom up: ", _bottom_up)
                    raise AttributeError("No positive evidence")

                layer.update(self_prediction, self_evidence, stats[layer.name], self_state[layer.name])
            else:
                # check belief overlap

                # set_self_prediction = set(_prediction.keys())
                set_self_evidence = set(_evidence.keys())
                set_other_evidence = set(other_state[layer.name].keys())  # need to get the string, not the object
                overlap_keys = list(set_self_evidence.intersection(set_other_evidence))

                # remember self_state P indices, so we can put them in the correct places
                overlapIDX2selfIDX = {idx:self_state[layer.name].id2idx[overlap] for idx, overlap in enumerate(overlap_keys)}

                # prepare overlaps for belief update with other
                # TODO: this currently would inhibit updates between prediction and evidence if the item is not in other!
                self_prediction = np.array([_prediction[overlap] for overlap in overlap_keys])
                # print("overlap keys: ", overlap_keys)
                # print("prediction keys: ", list(_prediction.keys()))
                # print("norm predictions: ", sum(_prediction.values()))
                # print("norm selected predictions: ", sum(self_prediction))
                self_evidence = np.array([_evidence[overlap] for overlap in overlap_keys])
                # print("norm evidence: ", sum(_evidence.values()))
                # print("norm selected evidence: ", sum(self_evidence))
                other_evidence = np.array([other_state[layer.name][overlap] for overlap in overlap_keys])
                # print("norm other evidence: ", sum(other_evidence))
                if max(other_evidence) == 0:
                    raise AttributeError("No positive ToM observation evidence")

                # print("evidence overlap({}):{}".format(len(overlap_keys), overlap_keys))
                # print("prediction({}):{}".format(len(set_self_prediction), set_self_prediction))
                # print("evidence for self({}):{} \nevidence for other({}):{}".format(len(set_self_evidence), set_self_evidence, len(set_other_evidence), set_other_evidence))

                # current state
                # print("prior:\n{}".format(self_state[layer.name].P))

                # TODO: update ToM_K from dominance and cooperation
                # stats["ToM_K"] = fn.kalman_gain(stats["dominance"], stats["cooperation"])  # not tested

                # Invert other
                if self.config["parameters"].get("Invert_other", False):
                    other_evidence = np.array([1-other_state[layer.name][overlap] for overlap in overlap_keys])
                    other_evidence /= sum(other_evidence)
                # run update, given the new information
                layer.update(self_prediction, self_evidence, stats[layer.name], self_state[layer.name], overlapIDX2selfIDX, other_evidence)  

                # before-after comparison
                # print("posterior:\n{}".format(self_state[layer.name].P))


            
            # stats = layer.update_params(stats[layer.name])

            self.last_layer = layer 

        # del self_state["orders"]
        # del self_state["world"]
        # print("state: ", self_state)
        # print("stats: ", stats)
        return self_state, stats


    def get_top_down_layer(self):
        layer = Layer("orders", "blue")
        return layer

    def get_next_layer(self, index):
        "Return the next layer in the hierarchy"
        if index < len(self.layers)-1:
            return self.layers[index+1]
        else:
            return Layer("world", "gray")


    def finalize(self):
        """ Call finalize method for each layer.
        """
        for layer in self.layers[::-1]:
            layer.finalize()
        
        # signal hierarchy processing stopping
        self.is_stopped = True
