#!/bin/bash

levels=("full-divider_salad" "partial-divider_salad" "open-divider_salad" "full-divider_tomato" "partial-divider_tomato" "open-divider_tomato" "full-divider_tl" "partial-divider_tl" "open-divider_tl")
levels=("full-divider_tl" "partial-divider_tl" "open-divider_tl")

sps=(0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0)

nagents=2
nseed=20

for seed in $(seq 1 1 $nseed); do
    for level in "${levels[@]}"; do
        for SP1 in "${sps[@]}"; do
            for SP2 in "${sps[@]}"; do
                echo python main.py --num-agents $nagents --seed $seed --level $level --model1 "HAICA" --SP1 $SP1 --model2 "HAICA" --SP2 $SP2
                python main.py --num-agents $nagents --seed $seed --level $level --model1 "HAICA" --SP1 $SP1 --model2 "HAICA" --SP2 $SP2
                sleep 5
            done
        done
    done
done
